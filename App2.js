import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    ListView,
    ActivityIndicator,
    TextInput,
    WebView,
    Button,
} from 'react-native';

const tmp = [];

export default class App2 extends Component<{}> {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            token: '',
            url: '',
            sid: '',
            number: '',
            from: '',
            text: '',
            captcha: '',
        }
    }

    componentDidMount() {
        return fetch('http://www.moldcell.md//rom/private/captcha/refresh/websms_main_form')
            .then((response) => response.json())
            .then((responseJson) => {
                let ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
                this.setState({
                    token: responseJson.data.token,
                    url: responseJson.data.url,
                    sid: responseJson.data.sid,
                    isLoading: false,
                    dataSource: ds.cloneWithRows(responseJson),
                }, function () {
                });
            })
            .catch((error) => {
                console.error(error);
            });
    }
    send() {
        fetch("http://www.moldcell.md/sendsms", {
            method: "POST",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                phone: this.state.number,
                name: this.state.from,
                message: this.state.text,
                captcha_sid: this.state.sid,
                captcha_token: this.state.token,
                captcha_response: this.state.captcha,
                conditions: 1,
                form_id: 'websms_main_form'
            })
        });
    }

    render() {
        if (this.state.isLoading) {
            return (
                <View style={{ flex: 1, paddingTop: 20 }}>
                    <ActivityIndicator />
                </View>
            );
        }

        return (
            <View style={{ flex: 1, paddingTop: 20 }}>
                <View >
                    <TextInput maxLength={8} editable={true} value={this.state.number} placeholder="Phone number" onChangeText={(number) => this.setState({ number })}></TextInput>
                    <TextInput maxLength={15} editable={true} value={this.state.from} placeholder="From" onChangeText={(from) => this.setState({ from })}></TextInput>
                    <TextInput style={{ height: 100 }} editable={true} maxLength={140} multiline={true} value={this.state.text} placeholder="Type message here" onChangeText={(text) => this.setState({ text })}></TextInput>
                </View>
                <View style={{ height: 48 }}>
                    <WebView
                        source={{ uri: 'http://www.moldcell.md/' + this.state.url }}
                        style={{ alignSelf: 'center', marginTop: 15, width: 76 }}
                    />
                </View>
                <TextInput maxLength={5} value={this.state.captcha} placeholder="Insert captcha" onChangeText={(captcha) => this.setState({ captcha })}></TextInput>
                <View >

                    <Button
                        onPress={this.send.bind(this)}
                        title="Send mesage"
                        color="#841584"
                        accessibilityLabel="Send"
                    />
                </View>
            </View>
        );
    }
}

